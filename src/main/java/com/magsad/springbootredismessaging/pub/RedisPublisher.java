package com.magsad.springbootredismessaging.pub;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.stereotype.Service;

@Service
@Slf4j
//@RequiredArgsConstructor
//@FieldDefaults(makeFinal = true,level = AccessLevel.PRIVATE)
public class RedisPublisher {

    @Autowired
    RedisTemplate<String,String> redisTemplate;
    @Autowired
    ChannelTopic topic;

    public void sendDataToRedisQueue(String data){
        redisTemplate.convertAndSend(topic.getTopic(),data);
        log.warn("Data : {} Redis Topic : {}",data,topic.getTopic());
    }

}
