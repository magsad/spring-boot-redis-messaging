package com.magsad.springbootredismessaging.config;
import java.util.UUID;

import com.magsad.springbootredismessaging.sub.RedisSubscriber;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.GenericToStringSerializer;

@Configuration
//@EnableConfigurationProperties(RedisProperties.class)
@Slf4j
public class RedisConfig {

    @Bean
    JedisConnectionFactory connectionFactory() {
        RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration("localhost", 6382);
        return new JedisConnectionFactory(redisStandaloneConfiguration);
    }

/*    @Bean
    LettuceConnectionFactory connectionFactory() {
        LettuceConnectionFactory connectionFactory = new LettuceConnectionFactory("localhost",6382);
        return new LettuceConnectionFactory();
    }*/

    @Bean
    public RedisTemplate<String, String> redisTemplate() {
        RedisTemplate<String, String> template = new RedisTemplate<>();
        template.setConnectionFactory(connectionFactory());
        template.setKeySerializer(new GenericToStringSerializer<String>(String.class));
//        template.setValueSerializer(new Jackson2JsonRedisSerializer<>(Object.class));
        return template;
    };

    @Bean
    MessageListenerAdapter messageListener() {
        return new MessageListenerAdapter(new RedisSubscriber());
    }

    @Bean
    ChannelTopic topic() {
        return new ChannelTopic(UUID.randomUUID().toString());
    }

    @Bean
    RedisMessageListenerContainer redisContainer() {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory());
        container.addMessageListener(new MessageListenerAdapter(new RedisSubscriber()), topic());
//        container.setTaskExecutor(Executors.newFixedThreadPool(4));
        return container;
    }
}