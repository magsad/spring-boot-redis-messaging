package com.magsad.springbootredismessaging;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRedisMessagingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRedisMessagingApplication.class, args);
	}

}
