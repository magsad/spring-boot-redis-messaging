package com.magsad.springbootredismessaging.controller;

import com.magsad.springbootredismessaging.pub.RedisPublisher;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/redis")
//@FieldDefaults(makeFinal = true,level = AccessLevel.PRIVATE)
//@RequiredArgsConstructor
public class RedisController {

    @Autowired
    RedisPublisher publisher;

    @GetMapping
    public String sendDataToRedisQueue(@RequestParam String input) {
        publisher.sendDataToRedisQueue(input);
        return "Successfully published!";
    }
}